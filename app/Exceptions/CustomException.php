<?php

namespace App\Exceptions;

use Illuminate\Http\Request;

class CustomException extends \Exception
{
    public function report()
    {

    }

    public function render(Request $request)
    {
        return response()->view(
            'errors.custom',
            array(
                'exception' => $this
            )
        );
    }


}
