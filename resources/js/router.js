import vueRouter from 'vue-router';
import Vue from 'vue';

Vue.use(vueRouter);

import Index from "./views/Index";
import Blog from "./views/Blog";

const routes = [
    {
        path: "/",
        component: Index
    },
    {
        path: "/blog",
        component: Blog
    },
    {
        path: "/posts/store"
    }
];

//конфигурация
export default new vueRouter({
    //все роуты будут сохраняться в истории
    mode: "history",
    routes: routes
});
